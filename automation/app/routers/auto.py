from colorsys import hls_to_rgb
from fastapi import APIRouter, HTTPException, Security, Depends
from fastapi.security.api_key import  APIKeyHeader
from pydantic import BaseModel, validator

from ..config import settings
from ..utils.support import fetch_instances, start_instance, get_status, stop_instance, start_instance_by_an_code
import hashlib
# from ..models.db_model import ports, database

router = APIRouter(
    prefix = "/code",
    responses = {404: {"description": "Not found"}},
)

# define API key -------------------------------------------
API_KEY = settings.api_key
API_KEY_NAME = "access_token"

api_key_header = APIKeyHeader(name=API_KEY_NAME, auto_error=False)

async def get_api_key(api_key_header: str = Security(api_key_header)):

    hash_api_key = hashlib.sha256(api_key_header.encode()).hexdigest()
    
    if hash_api_key == API_KEY:
        return api_key_header
    else:
        raise HTTPException(
            status_code=403, detail="Could not validate credentials"
        )
# ----------------------------------------------------------

class Validate(BaseModel):
    instance_id: str

    @validator('instance_id')
    def validate_email(cls, v):
        
        if not len(v) == 19:
            raise HTTPException(
            status_code=403, detail="Wrong instance ID"
        )
        return v


# Request body
class Item(BaseModel):
    an_code: str

# GET INSTANCES
@router.get("/instances", tags=["Instances"])
async def show_instances(APIKey = Depends(get_api_key)):

    return await fetch_instances()

# INSTANCES STATUS
@router.get("/instances/status/{instance_id}", tags=["Instances"])
async def instance_status(instance_id:  Validate = Depends(), APIKey = Depends(get_api_key)):
    
    return await get_status(instance_id=instance_id.instance_id)


# START INSTANCES
@router.post("/instances/start/{instance_id}", tags=["Instances"])
async def start_instances(instance_id:  Validate = Depends(), APIKey = Depends(get_api_key)):

    return await start_instance(instance_id=instance_id.instance_id)


# STOP INSTANCE
@router.post("/instances/stop/{instance_id}", tags=["Instances"])
async def stop_instances(instance_id:  Validate = Depends(), APIKey = Depends(get_api_key)):

    return await stop_instance(instance_id=instance_id.instance_id)


# START INSTANCE FROM AN-code
@router.post("/instances/an-code", tags=["Instances"])
async def start_instances_an_code(item: Item, APIkey = Depends(get_api_key)):

    return await start_instance_by_an_code(ancode=item.an_code)