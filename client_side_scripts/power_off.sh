#!/bin/bash

# service to kill Xwindow and power off the system

# wait for netrwork to connect
sleep 5
i=0
# check network connectivity
while true
do
http_code=`curl -Is www.google.com | head -1 | awk '{print $2}'`

if [[ "$http_code" != "200" ]]; then
echo "Waiting for network..."
sleep 2
i=`echo $i | awk '{print $1+1}'`
else
echo "**** Connected ****"
break
fi

# after 5 attempts power off
if [ $i -gt 5 ]; then
echo "No network found... Powering off..."
poweroff
fi
done


# catch True from mosquitto sub
resp=`mosquitto_sub -h net.dev.rebhu.com -p 8555 -t client_ss13ms110 -C 1 -q 2`

# condition to kill Xwindow
if [[ "$resp" == "0" ]]
then

poweroff

elif [[ "$resp" == "1" ]]
then
reboot

else
echo "No valid resp"
fi

