#!/bin/bash

# Script to automatically setup the client
# Inputs: -u <username>
# To run the script:
# install git: 
#   'sudo pacman -S git'
# clone the repo: 
#   'git clone https://gitlab.com/rebhu/rosf/thenexus-codes/scarlet/scripts.git/'
# run script: 
#   ./scripts/client_side_scripts/setup_client.sh

# -----------------------------------------------------------------

# ________________ GET COMMANDLINE ARGUMENT _______________________
usage() {
  echo -e "Usage: -u <username>"
}

while getopts u: flag
do
    case "${flag}" in
        u) user=${OPTARG};;
        *) usage
            exit 1 ;;
    esac
done

if [ -z "$user" ]
then
   usage
   exit
fi

# -----------------------------------------------------------------

# declare ANSI codes for color output
ERROR='\033[0;31m'
OK='\033[0;32m'
WARNING='\033[0;33m'
START='\033[0;34m'
END='\033[0m'

# Declare paths -------------------
plymouth_path="/usr/share/plymouth"
base_path="${HOME}/scripts/client_side_scripts"
logo_source="${base_path}/logo.png"
logo_target="${plymouth_path}/themes/materia-manjaro/"
theme_script="${logo_target}/script"
plymouth_defaults="${plymouth_path}/plymouthd.defaults"
plymouth_quit_service="/usr/lib/systemd/system/plymouth-quit.service"
getty_service="/usr/lib/systemd/system/getty@.service"
git_scarlet="https://gitlab.com/rebhu/rosf/thenexus-codes/scarlet/base"


# ____________ CHANGE BOOT LOGO ___________________________________

echo -e "\n[${START}START${END}]  Change boot logo\n"

# Add RebhuNet logo
sudo cp ${logo_source} ${logo_target}

# Edit theme script
sudo sed -i '21s/0.7/0.8/' ${theme_script}

# Edit plymouth defauts
sudo sed -i '3s/spinner/materia-manjaro/' ${plymouth_defaults}

# Install grub
sudo pacman -S grub

# Edit Plymouth-quit service
sudo sed -i '7i ExecStartPre=/bin/sleep 15' ${plymouth_quit_service}         # add line
sudo sed -i 's/20/16/' ${plymouth_quit_service}                              # replace 20 by 16
sudo sed -i '4s/Before/#Before/' ${plymouth_quit_service}                    # comment out Before
sudo sed -i '3s/$/\s getty@tty1.service/' ${plymouth_quit_service}           # add getty service at the end

# Edit getty service
sudo sed -i '14s/plymouth-quit-wait.service //' ${getty_service}             # remove plymouth service

# Update grub Daemon and initcpio
sudo systemctl daemon-reload
sudo mkinitcpio -P linux-rpi4

echo -e "[${OK}FINISH${END}] Change boot logo\n"


# ____________ ENABLE AUTO-LOGIN ___________________________________
echo -e "\n\n[${START}START${END}] Auto login\n"

# Comment out ExecStart
sudo sed -i '38s/ExecStart/#ExecStart/' ${getty_service}                      # comment out ExecStart

# Add new ExecStart
sudo sed -i "38i ExecStart=-/sbin/agetty --autologin ${user} --noclear %I \$TERM" ${getty_service}     # add new ExecStart

echo -e "[${OK}FINISH${END}] Auto login\n"


# ____________ INSTALL SCARLET _____________________________________
echo -e "\n\n[${START}START${END}] Install Scarlet\n"

# Install gpm
sudo pacman -S gpm
sudo systemctl enable gpm

# Git cline scarlet
mkdir scarlet
cd scarlet

git clone ${git_scarlet}
cd base
git checkout fix/upgrade

# Install required libraries
sudo bash ./install_libs.sh

cd ga
make all
make install
cd

echo -e "[${OK}FINISH${END}] Install Scarlet\n"


# ____________ ENABME POWER-OFF AND AUTO-START _________________________
echo -e "\n\n[${START}START${END}] Enable Power-Off and Auto-Start\n"

# Install Mosquitto
sudo pacman -S mosquitto

# copy scripts
mkdir .scarlet

# -------power-off
ln=`awk '/mosquitto_sub/{print NR}' ${base_path}/power_off.sh`

# Replace client name
sed "${ln}s/client_ss13ms110/client_${user}/" ${base_path}/power_off.sh > ~/.scarlet/power_off.sh

# -------auto-start
ln=`awk '/ga-client/{print NR}' ${base_path}/run_scarlet.sh`

# Take user input for RTSP path 
echo -e "${WARNING}Enter RTSP path [without rtsp://] e.g.${END} net.ss13ms110.rebhu.com"
read rtsp_path

# Replace client name
sed "${ln}s/net.ss13ms110.rebhu.com/${rtsp_path}/" ${base_path}/run_scarlet.sh > ~/.scarlet/run_scarlet.sh

# Edit bashrc
echo -e "\n# custom comments" >> ~/.bashrc
echo -e "bash ~/.scarlet/power_off.sh &" >> ~/.bashrc
echo -e "bash ~/.scarlet/run_scarlet.sh &" >> ~/.bashrc

echo -e "[${OK}FINISH${END}] Enable Power-Off and Auto-Start\n"
