# Rebhu Computing Contributing Guide

## Issue Reporting Guidelines
- Search available issues before creating a new one
- Add steps to reproduce errors
- Any feature requests should go in respective milestone's feature request
- Incomplete issues are closed automatically
- Do not flush issues with code. Use minimum amount of code to express the issue

## Pull/Merge Requests(PR) Guidelines
- The `master` branch is a snapshot of latest stable release. Never submit PRs against `master` branch.
- Checkout a topic branch and merge against it. In all other cases merge against `develop` branch.
- Create an issue before adding a new feature.
- When fixing a bug provide details of what you did to fix the bug. Along with this add issue number in the title.

## Development Guidelines
```bash
```

## Commenting Guidelines

Commenting patterns:
**This is a comment which is not good**

**{attribute}: Hola! you did it right this time**

Here is the list of {attribute}s:-

- TODO: You have skipped a part of code and you or your team will be coming back to it. TODOs don't affect the core functionality but add usability

- FIXME: Oh boy! You have treaded the unknown waters and have encountered a bug or who knows what. Add a FIXME beacon to let fellow coders understand what happened here and proceed further.

- OPTIMIZE: This part needs greasing it has been slowing the whole engine. Use OPTIMIZE when there is a scope of optimization in the code

- HACK: No, we don't have a security breach (but we can always face one so be aware, getting back). Use HACK if you think you or a fellow developer/team has used questionable programming practice which doesn't come up to our standards. Such steps are highly criticised. Such portions of code should be improved within two lifecycle(four weeks) of the app.

- REVIEW: Self-explanatory. Check if the code is performing as desired.

- README: Like documentation but not limited to it. You can write why you did something in the application or other related things.

- If your comment doesn't fall in any of the {attributes} then only you should use comments without attributes. In such scenario it is highly recommended, but not necessary, that you use [one or two words] inside square bracket to quickly identify the purpose your comment entered. 


## Happy developing brilliant products, cheers!!